class ErrorsCountChecker:
    def __init__(self, crash_message):
        self.count_Survival_Mod = crash_message.count("UnlimitedSurvivalMode.dll")
        self.count_buff_Achieve = crash_message.count("Achievements: true")
        self.count_Achieve_Mod = crash_message.count("achievements.dll")
        self.count_buff_Memory = crash_message.count("MemoryManager: true")
        self.count_Memory_Mod = crash_message.count("BakaScrapHeap.dll")
        self.count_buff_F4EE = crash_message.count("F4EE: false")
        self.count_F4EE_Mod = crash_message.count("f4ee.dll")
        self.count_Overflow = crash_message.count("EXCEPTION_STACK_OVERFLOW")
        self.count_ActiveEffect = crash_message.count("0x000100000000")
        self.count_BadMath = crash_message.count("EXCEPTION_INT_DIVIDE_BY_ZERO")
        self.count_Null = crash_message.count("0x000000000000")
        self.count_InvalidArg = crash_message.count("std::invalid_argument")
        self.count_KERNELBASE = crash_message.count("KERNELBASE.dll")
        self.count_MSVCP140 = crash_message.count("MSVCP140.dll")
        self.count_MSVCR110 = crash_message.count("MSVCR110.dll")
        self.count_CBP = crash_message.count("cbp.dll")
        self.count_skeleton = crash_message.count("skeleton.nif")
        self.count_DLCBanner01 = crash_message.count("DLCBannerDLC01.dds")
        self.count_BGSLocation = crash_message.count("BGSLocation")
        self.count_BGSQueued = crash_message.count("BGSQueuedTerrainInitialLoad")
        self.count_FaderData = crash_message.count("FaderData")
        self.count_FaderMenu = crash_message.count("FaderMenu")
        self.count_UIMessage = crash_message.count("UIMessage")
        self.count_BGSDecal = crash_message.count("BGSDecalManager")
        self.count_BSTempEffect = crash_message.count("BSTempEffectGeometryDecal")
        self.count_PipboyMapData = crash_message.count("PipboyMapData")
        self.count_Papyrus = crash_message.count("Papyrus")
        self.count_VirtualMachine = crash_message.count("VirtualMachine")
        self.count_CompileAndRun = crash_message.count("SysWindowCompileAndRun")
        self.count_NiBinaryStream = crash_message.count("BSResourceNiBinaryStream")
        self.count_ConsoleLogPrinter = crash_message.count("ConsoleLogPrinter")
        self.count_tbbmalloc = crash_message.count("tbbmalloc.dll")
        self.count_ParticleSystem = crash_message.count("ParticleSystem")
        self.count_LooseFileAsync = crash_message.count("LooseFileAsyncStream")
        self.count_d3d11 = crash_message.count("d3d11.dll")
        self.count_GridAdjacency = crash_message.count("GridAdjacencyMapNode")
        self.count_PowerUtils = crash_message.count("PowerUtils")
        self.count_LooseFileStream = crash_message.count("LooseFileStream")
        self.count_BSMulti = crash_message.count("BSMultiBoundNode")
        self.count_BSFade = crash_message.count("BSFadeNode")
        self.count_Create2DTexture = crash_message.count("Create2DTexture")
        self.count_DefaultTexture = crash_message.count("DefaultTexture")
        self.count_TextureBlack = crash_message.count("DefaultTexture_Black")
        self.count_NiAlphaProperty = crash_message.count("NiAlphaProperty")
        self.count_PathingCell = crash_message.count("PathingCell")
        self.count_BSPathBuilder = crash_message.count("BSPathBuilder")
        self.count_PathManagerServer = crash_message.count("PathManagerServer")
        self.count_NavMesh = crash_message.count("NavMesh")
        self.count_NavMeshObstacle = crash_message.count("BSNavmeshObstacleData")
        self.count_NavMeshDynamic = crash_message.count("DynamicNavmesh")
        self.count_bdhkm64 = crash_message.count("bdhkm64.dll")
        self.count_DeleteFileW = crash_message.count("usvfs::hook_DeleteFileW")
        self.count_X3DAudio1_7 = crash_message.count("X3DAudio1_7.dll")
        self.count_XAudio2_7 = crash_message.count("XAudio2_7.dll")
        self.count_BSMemStorage = crash_message.count("BSMemStorage")
        self.count_ReaderWriter = crash_message.count("DataFileHandleReaderWriter")
        self.count_Gamebryo = crash_message.count("GamebryoSequenceGenerator")
        self.count_BSD3D = crash_message.count("BSD3DResourceCreator")
        self.count_flexRelease_x64 = crash_message.count("flexRelease_x64.dll")
        self.count_nvwgf2umx = crash_message.count("nvwgf2umx.dll")
        self.count_SubmissionQueue = crash_message.count("DxvkSubmissionQueue")
        self.count_DXGIAdapter = crash_message.count("dxvk::DXGIAdapter")
        self.count_DXGIFactory = crash_message.count("dxvk::DXGIFactory")
        self.count_BSXAudio2Data = crash_message.count("BSXAudio2DataSrc")
        self.count_BSXAudio2Game = crash_message.count("BSXAudio2GameSound")
        self.count_Anim1 = crash_message.count("hkbVariableBindingSet")
        self.count_Anim2 = crash_message.count("hkbHandIkControlsModifier")
        self.count_Anim3 = crash_message.count("hkbBehaviorGraph")
        self.count_Anim4 = crash_message.count("hkbModifierList")
        self.count_DLCBanner05 = crash_message.count("DLCBanner05.dds")
        self.count_BGSAttachment = crash_message.count("BGSMod::Attachment")
        self.count_BGSTemplate = crash_message.count("BGSMod::Template")
        self.count_BGSTemplateItem = crash_message.count("BGSMod::Template::Item")
        self.count_BGSSaveBuffer = crash_message.count("BGSSaveFormBuffer")
        self.count_ButtonEvent = crash_message.count("ButtonEvent")
        self.count_MenuControls = crash_message.count("MenuControls")
        self.count_MenuOpenClose = crash_message.count("MenuOpenCloseHandler")
        self.count_PlayerControls = crash_message.count("PlayerControls")
        self.count_DXGISwapChain = crash_message.count("DXGISwapChain")
        self.count_BGSSaveManager = crash_message.count("BGSSaveLoadManager")
        self.count_BGSSaveThread = crash_message.count("BGSSaveLoadThread")
        self.count_INIMem1 = crash_message.count("+0CDAD30")
        self.count_INIMem2 = crash_message.count("+0D09AB7")
        self.count_Patrol = crash_message.count("BGSProcedurePatrol")
        self.count_PatrolExec = crash_message.count("BGSProcedurePatrolExecState")
        self.count_PatrolActor = crash_message.count("PatrolActorPackageData")
        self.count_TESObjectCELL = crash_message.count("TESObjectCELL")
        self.count_BGSStatic = crash_message.count("BGSStaticCollection")
        self.count_BGSCombined = crash_message.count("BGSCombinedCellGeometryDB")
        self.count_BSPacked = crash_message.count("BSPackedCombinedGeomDataExtra")
        self.count_HUDAmmo = crash_message.count("HUDAmmoCounter")
        self.count_BGSProjectile = crash_message.count("BGSProjectile")
        self.count_CombatProjectile = crash_message.count("CombatProjectileAimController")
        self.count_Player = crash_message.count("PlayerCharacter")
        self.count_0x7 = crash_message.count("0x00000007")
        self.count_0x8 = crash_message.count("0x00000008")
        self.count_0x14 = crash_message.count("0x00000014")
        self.count_LoadOrder = crash_message.count("[00]")
        self.count_Unofficial = crash_message.count("Unofficial Fallout 4 Patch.esp")
        self.count_CHW = crash_message.count("ClassicHolsteredWeapons")
        self.count_BSShadow = crash_message.count("BSShadowParabolicLight")
        self.count_BSShader = crash_message.count("BSShaderAccumulator")
        self.count_BSDFLight = crash_message.count("BSDFLightShader")
        self.count_UniquePlayer = crash_message.count("UniquePlayer.esp")
        self.count_BodyNIF = crash_message.count("Body.nif")
        self.count_HighHeels = crash_message.count("HHS.dll")
        self.count_FallSouls = crash_message.count("FallSouls.dll")
        self.count_F4SE = crash_message.count("f4se_1_10_163.dll")
        self.count_Module = crash_message.count("steam_api64.dll")


class ModsCountChecker:
    def __init__(self, type_str: str, message: str = "",
                 warn: str = "") -> None:
        self.type_str: str = type_str
        self.message: str = message
        self.warn = warn

    def __call__(self, line: str) -> bool:
        if "File" not in line and str("[FE") not in line and self.type_str in line:
            if self.message:
                print(line[0:5].strip(), self.message, self.warn)
                print("-----")
                return True
            return False
        elif "File" not in line and str("[FE") in line and self.type_str in line:
            if self.message:
                print(line[0:9].strip(), self.message, self.warn)
                print("-----")
                return True
            return False
