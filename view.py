import logging
import fnmatch
from pathlib import Path
from webbrowser import open_new
from configparser import ConfigParser
from model import crash_log_scanner
from tkinter import Tk, Frame, Label, IntVar, \
                    Checkbutton, Button, messagebox, mainloop
from tkinter.filedialog import askopenfilename
from tkinter.font import Font
from PIL.ImageTk import PhotoImage
from sys import path, platform
from os.path import join, normpath, getctime
if platform != 'win32':
    from os import listdir
    from subprocess import call
else:
    from os import startfile, listdir


class CrashLogScannerGUI:
    """Represents the interface and logic of the application."""
    def __init__(self) -> None:
        self.crashlogscanner_window: Tk = Tk()
        self.crashlogscanner_window.geometry('600x400+200+100')
        self.crashlogscanner_window.resizable(False, False)
        self.crashlogscanner_window.title('Crash Log Scanner GUI')

        self.settings: ConfigParser = ConfigParser()
        self.settings.read('CrashLogScannerGUI.ini')
        logging.basicConfig(
            filename='static/logs/debug.log',
            encoding='utf-8',
            level=logging.DEBUG
        )

        self.flag: int = int(
            self.settings['SETTINGS']['AUTO_OPEN_REPORT']
        )

        self.__build_icon_and_background_label()
        self.__build_frame()
        self.__build_flags()
        self.__build_buttons()
        self.__build_all_packs()

        mainloop()

    def __set_flag(self) -> None:
        """
        Responsible for setting the flag for automatically opening the report.
        """
        self.edit: ConfigParser = ConfigParser()
        self.edit.read('CrashLogScannerGUI.ini')
        if self.flag:
            self.flag = not self.flag
            self.edit['SETTINGS']['AUTO_OPEN_REPORT'] = '0'
        else:
            self.flag = not self.flag
            self.edit['SETTINGS']['AUTO_OPEN_REPORT'] = '1'
        with open('CrashLogScannerGUI.ini', 'w', errors='ignore') as settings:
            self.edit.write(settings)

    def __open_last_report(self) -> None:
        """Opens the last user report."""
        try:
            with open('static/logs/last_log.cfg', 'r', errors='ignore') as ll:
                line: list[str] = ll.readlines()
                filename: str = ('reports/' + line[0]).strip()
                if platform == "win32":
                    startfile(filename)
                elif platform == "darwin" or platform == "xdg-open":
                    opener = "open"
                    call([opener, filename])
        except FileNotFoundError as error:
            logging.debug(f'{error}')
            messagebox.showerror(
                title='Error!',
                message='File not found.'
            )

    def __discord_link(self) -> None:
        """Open authors Discord."""
        open_new(r'https://discord.com/invite/T3Vv3xr')

    def __get_buffout4_logs_path(self) -> str:
        """Return the path to the F4SE folder with different game logs.

        Returns:
            The full normalize path to the F4SE directory.
        """
        homepath: Path = Path.home()
        path_f4se: str = str(homepath) + r'/Documents/My Games/Fallout4/F4SE/'
        crashlog_path: str = normpath(path_f4se)
        return crashlog_path

    def __get_user_selected_file(self) -> None:
        """Opens the file selected and passes its to the handler."""
        buffout4_logs_path: str = self.__get_buffout4_logs_path()
        filetypes: tuple[tuple[str, str]] = (
            ('log files', '*.log'),
        )
        full_path_from_file_and_format: str = str(
            askopenfilename(
                title='Open a Buffout 4 .log file',
                initialdir=buffout4_logs_path,
                filetypes=filetypes
            )
        )
        if fnmatch.fnmatch(
                full_path_from_file_and_format[-29:], "crash-*.log"
        ):
            crash_log_scanner(full_path_from_file_and_format, self.flag)
        else:
            messagebox.showinfo(
                title='Information',
                message='This file is not a Buffout 4 log or '
                'file was not selected! '
                'Example of the log file: crash-2022-07-26-13-04-05.log.'
            )

    def __open_last_log(self) -> None:
        """
        Scans all files in the directory, determines the newest one
        and passes its functions to the handler.
        """
        crashlog_path: str = self.__get_buffout4_logs_path()
        files: list = []
        try:
            for file in listdir(crashlog_path):
                if fnmatch.fnmatch(file, "crash-*.log"):
                    files.append(crashlog_path + '\\' + file)
            full_path_from_file_and_format = max(files, key=getctime)
        except FileNotFoundError as error:
            logging.debug(f'{error}')
            messagebox.showerror(
                title='Error!',
                message='Files not found. Make' +
                'sure you have a path /Documents/My Games/Fallout4/F4SE/.'
            )
        except Exception as error:
            logging.debug(f'{error}')
        else:
            crash_log_scanner(full_path_from_file_and_format, self.flag)

    def __build_icon_and_background_label(self) -> None:
        """
        Responsible for installing the icon and background
        image of the application.
        """
        try:
            self.program_directory: str = path[0]
            self.image1: PhotoImage = PhotoImage(
                file='static/images/image.jpg'
            )
            self.background_label: Label = Label(
                image=self.image1
            )
            self.crashlogscanner_window.iconphoto(
                True, PhotoImage(file=join(
                    self.program_directory, "static/images/icon.ico"))
            )
        except Exception as error:
            logging.debug(f'{error}')

    def __build_frame(self) -> None:
        """Responsible for installing frames inside the application."""
        self.crashlogscanner_frame_top_1: Frame = Frame(
            self.crashlogscanner_window
        )
        self.crashlogscanner_frame_top_2: Frame = Frame(
            self.crashlogscanner_window
        )
        self.crashlogscanner_frame_top_3: Frame = Frame(
            self.crashlogscanner_window
        )
        self.crashlogscanner_frame_right_bottom: Frame = Frame(
            self.crashlogscanner_window
        )

    def __build_flags(self) -> None:
        """Responsible for setting the position of the flag."""
        self.auto_open_file_flag: IntVar = IntVar()
        self.auto_open_file_flag.set(self.flag)

    def __build_buttons(self) -> None:
        """Responsible for the installation of buttons."""
        self.text: Font = Font(family="Impact", size=14)
        self.scan_button: HoverButton = HoverButton(
            self.crashlogscanner_frame_top_1,
            text='Scan Selected Buffout Log',
            background='#555',
            foreground='#ccc',
            activebackground='#777',
            padx='10',
            pady='8',
            font=self.text,
            command=self.__get_user_selected_file
        )
        self.last_report_button: HoverButton = HoverButton(
            self.crashlogscanner_frame_top_2,
            text='Open Last Report',
            background='#555',
            foreground='#ccc',
            activebackground='#777',
            padx='46',
            pady='8',
            font=self.text,
            command=self.__open_last_report
        )
        self.last_log_button: HoverButton = HoverButton(
            self.crashlogscanner_frame_top_3,
            text='Auto-Scan Last Buffout Log',
            background='#555',
            foreground='#ccc',
            activebackground='#777',
            padx='8',
            pady='8',
            font=self.text,
            command=self.__open_last_log
        )
        self.discord_button: HoverButton = HoverButton(
            self.crashlogscanner_window,
            text='Author Discord',
            background='#555',
            foreground='#ccc',
            activebackground='#777',
            padx='19',
            pady='5',
            font=self.text,
            command=self.__discord_link
        )
        self.auto_open: Checkbutton = Checkbutton(
            self.crashlogscanner_frame_right_bottom,
            text='Auto Open Report',
            font=self.text,
            background='#555',
            foreground='#ccc',
            activebackground='#777',
            padx='0',
            pady='8',
            relief='raised',
            variable=self.auto_open_file_flag,
            command=self.__set_flag
        )

    def __build_all_packs(self) -> None:
        """Responsible for packing all the elements."""
        try:
            self.scan_button.pack(side='top')
            self.last_report_button.pack(side='top')
            self.last_log_button.pack(side='top')
            self.discord_button.place(x=0, y=400, anchor='sw')
            self.auto_open.pack(side='right')
            self.auto_open.bind("<Enter>", self.__on_enter)
            self.auto_open.bind("<Leave>", self.__on_leave)
            self.crashlogscanner_frame_top_1.pack()
            self.crashlogscanner_frame_top_3.pack()
            self.crashlogscanner_frame_top_2.pack()
            self.crashlogscanner_frame_right_bottom.place(
                x=600, y=400, anchor='se'
            )
            self.background_label.place(x=- 100, y=-10)
        except Exception as error:
            logging.debug(f'{error}')

    def __on_enter(self, event) -> None:
        self.auto_open.configure(background='#777')

    def __on_leave(self, event) -> None:
        self.auto_open.configure(background="#555")


class HoverButton(Button):
    """Inherits the base class of the button and creates a new view."""
    def __init__(self, master, **kw) -> None:
        Button.__init__(self, master=master, **kw)
        self.defaultBackground = self["background"]
        self.bind("<Enter>", self.__on_enter)
        self.bind("<Leave>", self.__on_leave)

    def __on_enter(self, e) -> None:
        self['background'] = self['activebackground']

    def __on_leave(self, e) -> None:
        self['background'] = self.defaultBackground
