def hello_info() -> None:
    print("This crash log was automatically scanned.")
    print("VER 1.3 | MIGHT CONTAIN FALSE POSITIVES.")
    print("====================================================")


def hello_info_checking_crash_messages() -> None:
    print("====================================================")
    print("CHECKING IF LOG MATCHES ANY KNOWN CRASH MESSAGES...")
    print("====================================================")


def hello_frequent_crashes() -> None:
    print("====================================================")
    print("CHECKING FOR MODS THAT CAN CAUSE FREQUENT CRASHES...")
    print("====================================================")
    print("IF YOU'RE USING DYNAMIC PERFORMANCE TUNER AND/OR LOAD ACCELERATOR,")
    print("remove these mods completely and switch to High FPS Physics Fix!")
    print("Link: https://www.nexusmods.com/fallout4/mods/44798?tab=files")
    print("-----")


def hello_dll_crash() -> None:
    print("[!]----------------------[!]----------------------[!]")
    print("MAIN ERROR REPORTS A DLL WAS INVLOVED IN THIS CRASH!")
    print("[!]----------------------[!]----------------------[!]")


def hello_community_patches() -> None:
    print("====================================================")
    print("CHECKING FOR MODS WITH SOLUTIONS & COMMUNITY PATCHES")
    print("====================================================")


def hello_specific() -> None:
    print("====================================================")
    print("SCANNING THE LOG FOR SPECIFIC (POSSIBLE) CUPLRITS...")
    print("====================================================")


def hello_patched_through_opc() -> None:
    print(
        "[Due to inherent limitations, Auto-Scan will continue detecting certain mods,"
    )
    print(" even if fixes or patches for them are already installed. You can ignore these.]")
    print("-----")
    print("FOR FULL LIST OF IMPORTANT PATCHES AND FIXES FOR THE BASE GAME AND MODS,")
    print("VISIT THIS ARTICLE: https://www.nexusmods.com/fallout4/articles/3769")
    print("-----")

    print("====================================================")
    print("CHECKING FOR MODS PATCHED THROUGH OPC INSTALLER...")
    print("====================================================")


def hello_authors() -> None:
    print("FOR FULL LIST OF MODS THAT CAUSE PROBLEMS, THEIR ALTERNATIVES AND DETAILED SOLUTIONS,")
    print("VISIT THE BUFFOUT 4 CRASH ARTICLE: https://www.nexusmods.com/fallout4/articles/3115")
    print("====================================================")
    print("DISCORD | Author/Made By: Poet#9800 & MakeHate#6753 | END OF SCAN")

    print("SCAN RESULTS ARE AVAILABE IN FILES NAMED crash-date-and-time-AUTOSCAN.md")
    print("===============================================================================")
    print("FOR FULL LIST OF MODS THAT CAUSE PROBLEMS, THEIR ALTERNATIVES AND DETAILED SOLUTIONS,")
    print("VISIT THE BUFFOUT 4 CRASH ARTICLE: https://www.nexusmods.com/fallout4/articles/3115")

    print("====================================== CONTACT INFO =====================================")
    print("END OF AUTOSCAN | DISCORD | Author/Made By: Poet#9800 & MakeHate#6753 | All Rights Reserved.")
    print("=========================================================================================")


def buff_ver_correct() -> None:
    print("You have the lastest version of Buffout 4!")


def buff_ver_incorrect() -> None:
    print("REPORTED BUFFOUT VERSION DOES NOT MATCH THE BUFFOUT VERSION USED BY AUTOSCAN")
    print("Update Buffout 4 if necessary: https://www.nexusmods.com/fallout4/mods/47359")


def buff_info() -> None:
    print("====================================================")
    print("CHECKING IF BUFFOUT4.TOML PARAMETERS ARE CORRECT...")
    print("====================================================")


def achivments_survival_incorrect() -> None:
    print(
        "CAUTION: Achievements Mod and/or Unlimited Survival Mode is installed, but Achievements parameter is "
        "set to TRUE")
    print(
        "FIX: Open Buffout4.toml and change Achievements parameter to FALSE, this prevents conflicts with "
        "Buffout 4.")
    print("-----")


def achivments_survoval_correct() -> None:
    print("Achievements parameter is correctly configured.")
    print("-----")


def baka_scrap_incorrect() -> None:
    print("CAUTION: Baka ScrapHeap is installed, but MemoryManager parameter is set to TRUE")
    print(
        "FIX: Open Buffout4.toml and change MemoryManager parameter to FALSE, this prevents conflicts with "
        "Buffout 4.")
    print(
        "You should also open BakaScrapHeap.toml with a text editor and change the ScrapHeapMult parameter to "
        "4.")
    print("-----")


def baka_scrap_correct() -> None:
    print("Memory Manager parameter is correctly configured.")
    print("-----")


def looks_menu_incorrect() -> None:
    print("CAUTION: Looks Menu is installed, but F4EE parameter under [Compatibility] is set to FALSE")
    print(
        "FIX: Open Buffout4.toml and change F4EE parameter to TRUE, this prevents bugs and crashes from Looks "
        "Menu.")
    print("-----")


def looks_menu_correct() -> None:
    print("Looks Menu (F4EE) parameter is correctly configured.")
    print("-----")


def count_Overflow_incorrect() -> None:
    print("Checking for Stack Overflow Crash.........CULPRIT FOUND!")
    print("> Priority Level: [5]")


def count_ActiveEffect_incorrect() -> None:
    print("Checking for Active Effects Crash.........CULPRIT FOUND!")
    print("> Priority Level: [5]")


def count_BadMath_incorrect() -> None:
    print("Checking for Bad Math Crash...............CULPRIT FOUND!")
    print("> Priority Level: [5]")


def count_Null_incorrect() -> None:
    print("Checking for Null Crash...................CULPRIT FOUND!")
    print("> Priority Level: [5]")


def count_CHW_1_incorrect() -> None:
    print("[!] Found: CLASSIC HOLSTERED WEAPONS")
    print("AUTOSCAN IS PRETTY CERTAIN THAT CHW CAUSED THIS CRASH!")
    print("You should disable CHW to further confirm this.")
    print("Visit the main crash logs article for additional solutions.")
    print("-----")


def count_CHW_2_incorrect() -> None:
    print("[!] Found: CLASSIC HOLSTERED WEAPONS")
    print("AUTOSCAN ALSO DETECTED ONE OR SEVERAL MODS THAT WILL CRASH WITH CHW.")
    print("You should disable CHW to further confirm it caused this crash.")
    print("Visit the main crash logs article for additional solutions.")
    print("-----")


def count_CHW_3_incorrect() -> None:
    print("[!] Found: CLASSIC HOLSTERED WEAPONS, BUT...")
    print("AUTOSCAN CANNOT ACCURATELY DETERMINE IF CHW CAUSED THIS CRASH OR NOT.")
    print("You should open CHW's ini file and change IsHolsterVisibleOnNPCs to 0.")
    print("This usually prevents most common crashes with Classic Holstered Weapons.")
    print("-----")


def count_Fallsouls_incorrect() -> None:
    print("[!] Found: FALLSOULS UNPAUSED GAME MENUS")
    print("Occasionally breaks the Quests menu, can cause crashes while changing MCM settings.")
    print("Advised Fix: Toggle PipboyMenu in FallSouls MCM settings or completely reinstall the mod.")
    print("-----")


def buff_trap_incorrect() -> None:
    print("-----")
    print("AUTOSCAN FOUND NO CRASH MESSAGES THAT MATCH THE CURRENT DATABASE.")
    print("Check below for mods that can cause frequent crashes and other problems.")


def buff_trap_correct() -> None:
    print("-----")
    print("FOR DETAILED DESCRIPTIONS AND POSSIBLE SOLUTIONS TO ANY ABOVE DETECTED CULPRITS,")
    print("CHECK THE 'HOW TO READ CRASH LOGS' PDF DOCUMENT INCLUDED WITH THE AUTO-SCANNER!")


def mods_trap_1() -> None:
    print("CAUTION: ANY ABOVE DETECTED MODS HAVE A MUCH HIGHER CHANCE TO CRASH YOUR GAME!")
    print("You can disable any/all of them temporarily to confirm they caused this crash.")
    print("-----")


def mods_trap_2() -> None:
    print("BUFFOUT 4 COULDN'T LOAD THE PLUGIN LIST FOR THIS CRASH LOG!")
    print("Autoscan cannot continue. Try scanning a different crash log.")
    print("-----")


def mods_trap_3() -> None:
    print("AUTOSCAN FOUND NO PROBLEMATIC MODS THAT MATCH THE CURRENT DATABASE FOR THIS LOG.")
    print("THAT DOESN'T MEAN THERE AREN'T ANY! YOU SHOULD RUN PLUGIN CHECKER IN WRYE BASH.")
    print("Wrye Bash Link: https://www.nexusmods.com/fallout4/mods/20032?tab=files")
    print("-----")


def mods_trap_4() -> None:
    print("BUFFOUT 4 COULDN'T LOAD THE PLUGIN LIST FOR THIS CRASH LOG!")
    print("Autoscan cannot continue. Try scanning a different crash log.")
    print("-----")


def mods_trap_5() -> None:
    print("Autoscan found no problematic mods with solutions and community patches.")
    print("-----")


def mods_trap_6() -> None:
    print("BUFFOUT 4 COULDN'T LOAD THE PLUGIN LIST FOR THIS CRASH LOG!")
    print("Autoscan cannot continue. Try scanning a different crash log.")
    print("-----")


def mods_trap_7() -> None:
    print("-----")
    print("FOR PATCH REPOSITORY THAT PREVENTS CRASHES AND FIXES PROBLEMS IN THESE AND OTHER MODS,")
    print("VISIT OPTIMIZATION PATCHES COLLECTION: https://www.nexusmods.com/fallout4/mods/54872")
    print("-----")


def mods_trap_8() -> None:
    print("Autoscan found no problematic mods that are already patched through OPC Installer.")
    print("-----")


def unofficial_patch_incorrect() -> None:
    print("UNOFFICIAL FALLOUT 4 PATCH ISN'T INSTALLED OR AUTOSCAN CANNOT DETECT IT!")
    print("If you own all DLCs, make sure that the Unofficial Patch is installed.")
    print("Link: https://www.nexusmods.com/fallout4/mods/4598?tab=files")
    print("-----")


def repeat_mods() -> None:
    print("If you have Depravity, Fusion City Rising, HOTC, Outcasts and Remnants and/or Project Valkyrie,")
    print("install this patch with facegen data, fully generated precomb/previs data and several tweaks.")
    print("Patch Link: https://www.nexusmods.com/fallout4/mods/56876?tab=files")
    print("-----")


def extender() -> None:
    print("AUTOSCAN CANNOT FIND FALLOUT 4 SCRIPT EXTENDER DLL!")
    print("MAKE SURE THAT F4SE IS CORRECTLY INSTALLED!")
    print("Link: https://f4se.silverlock.org/")
    print("-----")


def result_correct() -> None:
    print("AUTOSCAN COULDN'T FIND ANY PLUGIN CULRIPTS")
    print("-----")


def pl_result_incorrect() -> None:
    print("-----")
    print("These Plugins were caught by Buffout 4 and some of them might be responsible for this crash.")
    print("You can try disabling these plugins and recheck your game, though this method can be unreliable.")
    print("-----")


def detf_incorrect() -> None:
    print("-----")
    print("These Form IDs were caught by Buffout 4 and some of them might be related to this crash.")
    print("You can try searching any listed Form IDs in FO4Edit and see if they lead to relevant records.")
    print("-----")


def detf_incorrect_2() -> None:
    print("-----")
    print("These files were caught by Buffout 4 and some of them might be related to this crash.")
    print("Detected files in most cases appear as false positives, so no recommendation is given.")
    print("-----")
