from tkinter import messagebox
from sys import platform
if platform != 'win32':
    from subprocess import call
else:
    from os import startfile


class WorkConfirm:
    """Represents the end of actions with the scanned file."""    
    def __init__(self, result, report_filename, flag) -> None:
        """Sets all the necessary attributes for the object WorkConfirm.

        Args:
            result (bool): confirmation of successful scanning.
            report_filename (str): the name of the report file.
            flag (bool): that determines whether the report 
                         should be opened automatically after scanning.
        """        
        self.result = result
        self.flag = flag
        self.report_filename = report_filename

        if self.result:
            messagebox.showinfo(
                title='Scan completed!',
                message='The file with the result is located in the program folder.'
            )
        else:
            messagebox.showinfo(
                title='Scan fail!',
                message='Check reason in debug file.'
            )

        if self.flag:
            auto_open: str = ('reports/' + self.report_filename).strip()
            if platform == "win32":
                startfile(auto_open)
            elif platform == "darwin" or platform == "xdg-open":
                opener = "open"
                call([opener, auto_open])
