# Buffout4 - Crash Log Scanner GUI.
This scanner is based on the version published on Nexus, authored by Poet: [Original](https://www.nexusmods.com/fallout4/mods/56255)

# Under the hood:
- [Python](http://python.org/)
- [Tkinter](https://docs.python.org/3/library/tkinter.html)
- [Pillow](https://pillow.readthedocs.io/en/stable/#)

# How is it different from the original:
- No longer requires Python installation;
- It has a graphical interface;
- Works on Linux and on Windows;
- Allows you to select the desired file for analysis;
- Allows you to open the last scanned report;
- Allows you to open the last log file by the date of its creation;
- A lot of work has been done on decomposition.

# Requirements:
- Pillow==9.4.0

# How launch:
- Run the file main.py.

# Important:
- Based on an old version of the original, so as not to compete with it for attention.

# Screenshot:
![insert_image1](https://staticdelivery.nexusmods.com/mods/1151/images/63346/63346-1661246457-289416316.png)
