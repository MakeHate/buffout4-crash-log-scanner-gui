List_Mods1 = [" DamageThresholdFramework.esm",
                      " Endless Warfare.esm",
                      " ExtendedWeaponSystem.esm",
                      " EPO",
                      " SakhalinWasteland",
                      " 76HUD",
                      " NCRenegade",
                      " Respawnable Legendary Bosses",
                      " Scrap Everything",
                      " Shade Girl Leather Outfits",
                      " SpringCleaning.esm",
                      " (STO) NO",
                      " TacticalTablet.esp",
                      " True Nights",
                      " WeaponsFramework",
                      " WOTC.esp"]

List_Warn1 = ["DAMAGE THRESHOLD FRAMEWORK \n"
                      "- Can cause crashes in combat on some occasions due to how damage calculations are done.",
                      
                      "ENDLESS WARFARE \n"
                      "- Some enemy spawn points could be bugged or crash the game due to scripts or pathfinding.",
                      
                      "EXTENDED WEAPON SYSTEMS \n"
                      "- Alternative to Tactical Reload that suffers from similar weapon related problems and crashes.",
                      
                      "EXTREME PARTICLES OVERHAUL \n"
                      "- Can cause particle effects related crashes, its INI file raises particle count to 500000. \n"
                      "  Consider switching to Burst Impact Blast FX: https://www.nexusmods.com/fallout4/mods/57789",
                      
                      "FALLOUT SAKHALIN \n"
                      "- Breaks the precombine system all across Far Harbor which will randomly crash your game.",
                      
                      "HUD76 HUD REPLACER \n"
                      "- Can sometimes cause interface and pip-boy related bugs, glitches and crashes.",
                      
                      "NCR RENEGADE ARMOR \n"
                      "- Broken outfit mesh that crashes the game in 3rd person or when NPCs wearing it are hit.",
                      
                      "RESPAWNABLE LEGENDARY BOSSES \n"
                      "- Can sometimes cause Deathclaw \ Behmoth boulder projectile crashes for unknown reasons.",
                      
                      "SCRAP EVERYTHING \n"
                      "- Weird crashes and issues due to multiple unknown problems. This mod must be always last in your load order.",
                      
                      "SHADE GIRL LEATHER OUTFITS \n"
                      "- Outfits can crash the game while browsing the armor workbench or upon starting a new game due to bad meshes.",
                      
                      "SPRING CLEANING \n"
                      "- Abandoned and severely outdated mod that breaks precombines and could potentially even break your save file.",
                      
                      "STALKER TEXTURE OVERHAUL \n"
                      "- Doesn't work due to incorrect folder structure and has a corrupted dds file that causes Create2DTexture crashes.",  
                      
                      "TACTICAL TABLET \n"
                      "- Can cause flickering with certain scopes or crashes while browsing workbenches, most commonly with ECO.",
                      
                      "TRUE NIGHTS \n"
                      "- Has an invalid Image Space Adapter (IMAD) Record that will corrupt your save memory and has to be manually fixed.",
                      
                      "WEAPONS FRAMEWORK BETA \n"
                      "- Will randomly cause crashes when used with Tactical Reload and possibly other weapon or combat related mods. \n"
                      "  Visit Important Patches List article for possible solutions: https://www.nexusmods.com/fallout4/articles/3769",
                      
                      "WAR OF THE COMMONWEALTH \n"
                      "- Seems responsible for consistent crashes with specific spawn points or randomly during settlement attacks."]

#Needs 1 empty space as prefix to prevent duplicates.
List_Mods2 = [" DLCUltraHighResolution.esm",
                      " AAF.esm",
                      " ArmorKeywords.esm",
                      " BTInteriors_Project.esp",
                      " CombatZoneRestored",
                      " D.E.C.A.Y.esp",
                      " EveryonesBestFriend",
                      " M8r_Item_Tags",
                      " Fo4FI_FPS_fix",
                      " BostonFPSFix",
                      " FunctionalDisplays.esp",
                      " skeletonmaleplayer",
                      " skeletonfemaleplayer",
                      " CapsWidget",
                      " Homemaker.esm",
                      " LegendaryModification.esp",
                      " MilitarizedMinutemen.esp",
                      " MoreUniques",
                      " RaiderOverhaul.esp",
                      " SKKCraftableWeaponsAmmo",
                      " SOTS.esp",
                      " StartMeUp.esp",
                      " SuperMutantRedux.esp",
                      " TacticalReload.esm",
                      " Creatures and Monsters.esp",
                      " ZombieWalkers"]
        
List_Warn2 = ["HIGH RESOLUTION DLC. I STRONGLY ADVISE NOT USING IT! \n"
                      "Right click on Fallout 4 in your Steam Library folder, then select Properties \n"
                      "Switch to the DLC tab and uncheck / disable the High Resolution Texture Pack",
                      #
                      "ADVANCED ANIMATION FRAMEWORK \n"
                      "Looks Menu versions 1.6.20 & 1.6.19 can frequently break adult mod related (erection) morphs. \n"
                      "If you notice any AAF realted problems, uninstall latest version of Looks Menu and switch to 1.6.18.",
                      #
                      "ARMOR AND WEAPON KEYWORDS \n"
                      "If you don't rely on AWKCR, you should switch to Equipment and Crafting Overhaul \n"
                      "Better Alternative: https://www.nexusmods.com/fallout4/mods/55503?tab=files",
                      #
                      "BEANTOWN INTERIORS PROJECT \n"
                      "Usually causes fps drops, stuttering, crashing and culling issues in multiple locations. \n"
                      "Patch Link: https://www.nexusmods.com/fallout4/mods/53894?tab=files",
                      #
                      "COMBAT ZONE RESTORED \n"
                      "Contains few small issues and NPCs usually have trouble navigating the interior space. \n"
                      "Patch Link: https://www.nexusmods.com/fallout4/mods/59329?tab=files",
                      #
                      "DECAY BETTER GHOULS \n"
                      "You have to install DECAY Redux patch to prevent its audio files from crashing the game. \n"
                      "Patch Link: https://www.nexusmods.com/fallout4/mods/59025?tab=files",
                      #
                      "EVERYONE'S BEST FRIEND \n"
                      "This mod needs a compatibility patch to properly work with the Unofficial Patch (UFO4P). \n"
                      "Patch Link: https://www.nexusmods.com/fallout4/mods/43409?tab=files",
                      #
                      "FALLUI ITEM SORTER (OLD) \n"
                      "This is an outdated item tagging / sorting patch that can cause crashes or conflicts in all kinds of situations. \n"
                      "I strongly recommend to instead generate your own sorting patch and place it last in your load order. \n"
                      "That way, you won't experience any conflicts / crashes and even modded items will be sorted. \n"
                      "Generate Sorting Patch With This: https://www.nexusmods.com/fallout4/mods/48826?tab=files",
                      #
                      "FO4FI FPS FIX \n"
                      "This mod is severely outdated and will cause crashes even with compatibility patches. \n"
                      "Better Alternative: https://www.nexusmods.com/fallout4/mods/46403?tab=files",
                      #
                      "BOSTON FPS FIX \n"
                      "This mod is severely outdated and will cause crashes even with compatibility patches. \n"
                      "Better Alternative: https://www.nexusmods.com/fallout4/mods/46403?tab=files",
                      #
                      "FUNCTIONAL DISPLAYS \n"
                      "Frequently causes object model (nif) related crashes and this needs to be manually corrected. \n"
                      "Advised Fix: Open its Meshes folder and delete everything inside EXCEPT for the Functional Displays folder.",
                      #
                      "GENDER SPECIFIC SKELETONS (MALE) \n"
                      "High chance to cause a crash when starting a new game or during the game intro sequence. \n"
                      "Advised Fix: Enable the mod only after leaving Vault 111. Existing saves shouldn't be affected.",
                      #
                      "GENDER SPECIFIC SKELETONS (FEMALE) \n"
                      "High chance to cause a crash when starting a new game or during the game intro sequence. \n"
                      "Advised Fix: Enable the mod only after leaving Vault 111. Existing saves shouldn't be affected.",
                      #
                      "HUD CAPS \n"
                      "Often breaks the Save / Quicksave function due to poor script implementation. \n"
                      "Advised Fix: Download fixed pex file and place it into HUDCaps/Scripts folder. \n"
                      "Fix Link: https://drive.google.com/file/d/1egmtKVR7mSbjRgo106UbXv_ySKBg5az2/view",
                      #
                      "HOMEMAKER \n"
                      "Causes a crash while scrolling over Military / BoS fences in the Settlement Menu. \n"
                      "Patch Link: https://www.nexusmods.com/fallout4/mods/41434?tab=files",
                      #
                      "LEGENDARY MODIFICATION \n"
                      "Old mod plagued with all kinds of bugs and crashes, can conflict with some modded weapons. \n"
                      "Better Alternative: https://www.nexusmods.com/fallout4/mods/55503?tab=files",
                      #
                      "MILITARIZED MINUTEMEN \n"
                      "Can occasionally crash the game due to a broken mesh on some minutemen outfits. \n"
                      "Patch Link: https://www.nexusmods.com/fallout4/mods/55301?tab=files",
                      #
                      "MORE UNIQUE WEAPONS EXPANSION \n"
                      "Causes crashes due to broken precombines and compatibility issues with other weapon mods. \n"
                      "Patch Link: https://www.nexusmods.com/fallout4/mods/54848?tab=files",
                      #
                      "RAIDER OVERHAUL \n"
                      "Old mod that requires several patches to function as intended. Use ONE Version instead. \n"
                      "Upated ONE Version: https://www.nexusmods.com/fallout4/mods/51658?tab=files",
                      #
                      "SKK CRAFT WEAPONS AND SCRAP AMMO \n"
                      "Version 008 is incompatible with AWKCR and will cause crashes while saving the game. \n"
                      "Advised Fix: Use Version 007 or remove AWKCR and switch to Equipment and Crafting Overhaul.",
                      #
                      "SOUTH OF THE SEA \n"
                      "Very unstable mod that consistently and frequently causes strange problems and crashes. \n"
                      "Patch Link: https://www.nexusmods.com/fallout4/mods/59792?tab=files",
                      #
                      "START ME UP \n"
                      "Abandoned mod that can cause infinite loading and other problems. Use REDUX Version instead. \n"
                      "Upated REDUX Version: https://www.nexusmods.com/fallout4/mods/56984?tab=files",
                      #
                      "SUPER MUTANT REDUX \n"
                      "Causes crashes at specific locations or with certain Super Muntant enemies and items. \n"
                      "Patch Link: https://www.nexusmods.com/fallout4/mods/51353?tab=files",
                      #
                      "TACTICAL RELOAD \n"
                      "Can cause weapon and combat related crashes. TR Expansion For ECO is highly recommended. \n"
                      "TR Expansion For ECO Link: https://www.nexusmods.com/fallout4/mods/62737",
                      #
                      "UNIQUE NPCs CREATURES AND MONSTERS \n"
                      "Causes crashes and breaks precombines at specific locations, some creature spawns are too frequent. \n"
                      "Patch Link: https://www.nexusmods.com/fallout4/mods/48637?tab=files",
                      #
                      "ZOMBIE WALKERS \n"
                      "Version 2.6.3 contains a resurrection script that will regularly crash the game. \n"
                      "Advised Fix: Make sure you're using the 3.0 Beta version of this mod or newer."]

        #Needs 1 empty space as prefix to prevent duplicates.
List_Mods3 = [" Beyond the Borders",
                      " Deadly Commonwealth Expansion",
                      " Dogmeat and Strong Armor",
                      " DoYourDamnJobCodsworth",
                      " ConcordEXPANDED",
                      " HagenEXPANDED",
                      " GlowingSeaEXPANDED",
                      " SalemEXPANDED",
                      " SwampsEXPANDED",
                      " _hod",
                      " ImmersiveBeantown",
                      " CovenantComplex",
                      " GunnersPlazaInterior",
                      " ImmersiveHubCity",
                      " Immersive_Lexington",
                      " Immersive Nahant",
                      " Immersive S Boston",
                      " MutilatedDeadBodies",
                      " Vault4",
                      " atlanticofficesf23",
                      " Minutemen Supply Caches",
                      " moreXplore",
                      " NEST_BUNKER_PROJECT",
                      " Raider Children.esp",
                      " sectorv",
                      " SettlementShelters",
                      " subwayrunnnerdynamiclighting",
                      " 3DNPC_FO4Settler.esp",
                      " 3DNPC_FO4.esp",
                      " The Hollow",
                      " nvvault1080",
                      " Vertibird Faction Paint Schemes",
                      " MojaveImports.esp",
                      " Firelance2.5",
                      " zxcMicroAdditions"]
        
List_Warn3 = ["Beyond the Borders",
                      "Deadly Commonwealth Expansion",
                      "Dogmeat and Strong Armor",
                      "Do Your Damn Job Codsworth",
                      "Concord Expanded",
                      "Fort Hagen Expanded",
                      "Glowing Sea Expanded",
                      "Salem Expanded",
                      "Swamps Expanded",
                      "Hearts Of Darkness",
                      "Immersive Beantown Brewery",
                      "Immersive Covenant Compound",
                      "Immersive Gunners Plaza",
                      "Immersive Hub City",
                      "Immersive & Extended Lexington",
                      "Immersive & Extended Nahant",
                      "Immersive Military Checkpoint",
                      "Mutilated Dead Bodies",
                      "Fourville (Vault 4)",
                      "Lost Building of Atlantic",
                      "Minutemen Supply Caches",
                      "MoreXplore",
                      "NEST Survival Bunkers",
                      "Raider Children & Other Horrors",
                      "Sector Five - Rise and Fall",
                      "Settlement Shelters",
                      "Subway Runner (Dynamic Lights)",
                      "Settlers of the Commonwealth",
                      "Tales from the Commonwealth",
                      "The Hollow",
                      "Vault 1080 (Vault 80)",
                      "Vertibird Faction Paint Schemes",
                      "Wasteland Imports (Mojave Imports)",
                      "Xander's Aid",
                      "ZXC Micro Additions"]