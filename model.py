import sys
import prints
from os.path import exists
from confirm import WorkConfirm
from pathlib import Path
from subprocess import check_call
from checkers import ModsCountChecker, ErrorsCountChecker
from mods import List_Mods1, List_Mods2, List_Mods3, \
    List_Warn1, List_Warn2, List_Warn3


def get_md_file_name(log_file_name: str, format_name: str):
    """Returns the name of the report file.

    Args:
        log_file_name (str): the name of the scanned file without the format.
        format_name (str): file format -AUTOSCAN.md

    Returns:
        (str): the name of the report file
    """    
    return log_file_name + format_name


def after_scanning(result: bool, report_filename: str, flag: bool) -> None:
    """Writing the name of the last scanned file to the configuration file.

    Args:
        result (bool): confirmation of successful scanning.
        report_filename (str): the name of the report file.
        flag (bool): that determines whether the report 
                     should be opened automatically after scanning.
    """
    platform: str = sys.platform
    with open('static/logs/last_log.cfg', 'a', errors='ignore') as ll:
        ll.write(report_filename + '\n')
        WorkConfirm(result, report_filename, flag)
    if exists('static/logs/last_log.cfg') and platform == 'win32':
        check_call(["attrib","+H","static/logs/last_log.cfg"])


def crash_log_scanner(log_file, flag) -> None:
    """Scans the file for errors.

    Args:
        log_file (string): the full path to the file.
        flag (bool): that determines whether the report 
                     should be opened automatically after scanning.
    """    
    checker = False
    correct_file_name: str = Path(log_file).resolve().stem + '.log'
    correct_file_name_without_format: str = Path(log_file).resolve().stem

    with open(log_file, 'r', errors="ignore") as crash_version:
        orig_stdout = sys.stdout
        md_file_name = get_md_file_name(correct_file_name_without_format, "-AUTOSCAN.md")
        sys.stdout = open(f'reports/{md_file_name}', "w", errors="ignore")

        all_lines = crash_version.readlines()

        buff_ver = all_lines[1].strip()
        buff_error = all_lines[3].strip()

        print(correct_file_name)
        prints.hello_info()

        buff_latest = "Buffout 4 v1.26.2"
        print("Main Error:", buff_error)
        print("====================================================")
        print("Detected Buffout Version:", buff_ver.strip())
        print("Latest Buffout Version:", buff_latest.strip())

        if buff_ver.casefold() == buff_latest.casefold():
            prints.buff_ver_correct()
        else:
            prints.buff_ver_incorrect()

    with open(log_file, 'r', errors="ignore") as crash_log:
        crash_message = crash_log.read()
        errors = ErrorsCountChecker(crash_message)
        prints.buff_info()
        if errors.count_buff_Achieve and errors.count_Achieve_Mod >= 1 or errors.count_buff_Achieve and errors.count_Survival_Mod >= 1:
            prints.achivments_survival_incorrect()
        else:
            prints.achivments_survoval_correct()
        if errors.count_buff_Memory and errors.count_Memory_Mod >= 1:
            prints.baka_scrap_incorrect()
        else:
            prints.baka_scrap_correct()
        if errors.count_buff_F4EE and errors.count_F4EE_Mod >= 1:
            prints.looks_menu_incorrect()
        else:
            prints.looks_menu_correct()
            
        prints.hello_info_checking_crash_messages()
        Buffout_Trap = 1

        if ".dll" in buff_error and "tbbmalloc" not in buff_error:
            prints.hello_dll_crash()
        if errors.count_Overflow >= 1:
            prints.count_Overflow_incorrect()
            Buffout_Trap = 0
        if errors.count_ActiveEffect >= 1:
            prints.count_ActiveEffect_incorrect()
            Buffout_Trap = 0
        if errors.count_BadMath >= 1:
            prints.count_BadMath_incorrect()
            Buffout_Trap = 0
        if errors.count_Null >= 1:
            prints.count_Null_incorrect()
            Buffout_Trap = 0
        if errors.count_DLCBanner01 >= 1:
            print("Checking for DLL Crash....................CULPRIT FOUND!")
            print("> Priority Level: [5] | DLCBannerDLC01.dds : ",errors.count_DLCBanner01)
            Buffout_Trap = 0
        if (errors.count_BGSLocation and errors.count_BGSQueued) >= 1:
            print("Checking for LOD Crash....................CULPRIT FOUND!")
            print("> Priority Level: [5] | BGSLocation : ",errors.count_BGSLocation," | BGSQueuedTerrainInitialLoad : ",errors.count_BGSQueued)
            Buffout_Trap = 0
        if (errors.count_FaderData or errors.count_FaderMenu or errors.count_UIMessage) >= 1:
            print("Checking for MCM Crash....................CULPRIT FOUND!")
            print("> Priority Level: [3] | FaderData : ",errors.count_FaderData," | FaderMenu : ",errors.count_FaderMenu," | UIMessage : ",errors.count_UIMessage)
            Buffout_Trap = 0
        if (errors.count_BGSDecal or errors.count_BSTempEffect) >= 1:
            print("Checking for Decal Crash..................CULPRIT FOUND!")
            print("> Priority Level: [5] | BGSDecalManager : ",errors.count_BGSDecal," | BSTempEffectGeometryDecal : ",errors.count_BSTempEffect)
            Buffout_Trap = 0
        if errors.count_PipboyMapData >= 2:
            print("Checking for Equip Crash..................CULPRIT FOUND!")
            print("> Priority Level: [2] | PipboyMapData : ",errors.count_PipboyMapData)
            Buffout_Trap = 0            
        if (errors.count_Papyrus or errors.count_VirtualMachine) >= 2:
            print("Checking for Script Crash.................CULPRIT FOUND!")
            print("> Priority Level: [3] | Papyrus : ",errors.count_Papyrus," | VirtualMachine : ",errors.count_VirtualMachine)
            Buffout_Trap = 0
        if errors.count_tbbmalloc >= 3 or "tbbmalloc" in buff_error:
            print("Checking for Generic Crash................CULPRIT FOUND!")
            print("> Priority Level: [2] | tbbmalloc.dll : ",errors.count_tbbmalloc)
            Buffout_Trap = 0
        if errors.count_LooseFileAsync >= 1:
            print("Checking for BA2 Limit Crash..............CULPRIT FOUND!")
            print("> Priority Level: [5] | LooseFileAsyncStream : ",errors.count_LooseFileAsync)
            Buffout_Trap = 0
        if errors.count_d3d11 >= 3 or "d3d11" in buff_error:
            print("Checking for Rendering Crash..............CULPRIT FOUND!")
            print("> Priority Level: [4] | d3d11.dll : ",errors.count_d3d11)
            Buffout_Trap = 0
        if (errors.count_GridAdjacency or errors.count_PowerUtils) >= 1:
            print("Checking for Grid Scrap Crash.............CULPRIT FOUND!")
            print("> Priority Level: [5] | GridAdjacencyMapNode : ",errors.count_GridAdjacency," | PowerUtils : ",errors.count_PowerUtils)
            Buffout_Trap = 0
        if (errors.count_LooseFileStream or errors.count_BSFade or errors.count_BSMulti) >= 1 and errors.count_LooseFileAsync == 0:
            print("Checking for Mesh (NIF) Crash.............CULPRIT FOUND!")
            print("> Priority Level: [4] | LooseFileStream : ",errors.count_LooseFileStream," | BSFadeNode : ",errors.count_BSFade," | BSMultiBoundNode : ",errors.count_BSMulti)
            Buffout_Trap = 0
        if (errors.count_Create2DTexture or errors.count_DefaultTexture) >= 1:
            print("Checking for Texture (DDS) Crash..........CULPRIT FOUND!")
            print("> Priority Level: [3] | Create2DTexture : ",errors.count_Create2DTexture," | DefaultTexture : ",errors.count_DefaultTexture)
            Buffout_Trap = 0
        if (errors.count_TextureBlack or errors.count_NiAlphaProperty) >= 1:
            print("Checking for Material (BGSM) Crash........CULPRIT FOUND!")
            print("> Priority Level: [3] | DefaultTexture_Black : ",errors.count_TextureBlack," | NiAlphaProperty : ",errors.count_NiAlphaProperty)
            Buffout_Trap = 0
        if (errors.count_bdhkm64 or errors.count_DeleteFileW) >= 2:
            print("Checking for BitDefender Crash............CULPRIT FOUND!")
            print("> Priority Level: [5] | bdhkm64.dll : ",errors.count_bdhkm64," | usvfs::hook_DeleteFileW : ",errors.count_DeleteFileW)
            Buffout_Trap = 0
        if (errors.count_PathingCell or errors.count_BSPathBuilder or errors.count_PathManagerServer) >= 1:
            print("Checking for NPC Pathing Crash............CULPRIT FOUND!")
            print("> Priority Level: [3] | PathingCell : ",errors.count_PathingCell," | BSPathBuilder : ",errors.count_BSPathBuilder," | PathManagerServer : ",errors.count_PathManagerServer)
            Buffout_Trap = 0
        elif (errors.count_NavMesh or errors.count_NavMeshObstacle or errors.count_NavMeshDynamic) >= 1:
            print("Checking for NPC Pathing Crash............CULPRIT FOUND!")
            print("> Priority Level: [3] | NavMesh : ",errors.count_NavMesh," | BSNavmeshObstacleData : ",errors.count_NavMeshObstacle," | DynamicNavmesh : ",errors.count_NavMeshDynamic)
            Buffout_Trap = 0
        if errors.count_X3DAudio1_7 >= 3 or errors.count_XAudio2_7 >= 2 or "X3DAudio1_7" in buff_error or "XAudio2_7" in buff_error:
            print("Checking for Audio Driver Crash...........CULPRIT FOUND!")
            print("> Priority Level: [5] | X3DAudio1_7.dll : ",errors.count_X3DAudio1_7," | XAudio2_7.dll : ",errors.count_XAudio2_7)
            Buffout_Trap = 0
        if errors.count_CBP >= 3 or errors.count_skeleton >= 1 or "cbp" in buff_error:
            print("Checking for Body Physics Crash...........CULPRIT FOUND!")
            print("> Priority Level: [4] | cbp.dll : ",errors.count_CBP," | skeleton.nif : ",errors.count_skeleton)
            Buffout_Trap = 0 
        if (errors.count_BSMemStorage or errors.count_ReaderWriter) >= 1:
            print("Checking for Plugin Limit Crash...........CULPRIT FOUND!")
            print("> Priority Level: [5] | BSMemStorage : ",errors.count_BSMemStorage," | DataFileHandleReaderWriter : ",errors.count_ReaderWriter)
            Buffout_Trap = 0 
        if errors.count_Gamebryo >= 1:
            print("Checking for Plugin Order Crash...........CULPRIT FOUND!")
            print("> Priority Level: [5] | GamebryoSequenceGenerator : ",errors.count_Gamebryo)
            Buffout_Trap = 0
        if errors.count_BSD3D == 3 or errors.count_BSD3D == 6:
            print("Checking for MO2 Extractor Crash..........CULPRIT FOUND!")
            print("> Priority Level: [5] | BSD3DResourceCreator : ",errors.count_BSD3D)
            Buffout_Trap = 0
        if errors.count_flexRelease_x64 >= 2 or "flexRelease_x64" in buff_error:
            print("Checking for Nvidia Debris Crash..........CULPRIT FOUND!")
            print("> Priority Level: [5] | flexRelease_x64.dll : ",errors.count_flexRelease_x64)
            Buffout_Trap = 0 
        if errors.count_nvwgf2umx >= 10 or "nvwgf2umx" in buff_error:
            print("Checking for Nvidia Driver Crash..........CULPRIT FOUND!")
            print("> Priority Level: [5] | nvwgf2umx.dll : ",errors.count_nvwgf2umx)
            Buffout_Trap = 0
        if (errors.count_KERNELBASE or errors.count_MSVCP140) >= 3 and errors.count_SubmissionQueue >= 1:
            print("Checking for Vulkan Memory Crash..........CULPRIT FOUND!")
            print("> Priority Level: [5] | KERNELBASE.dll : ",errors.count_KERNELBASE," | MSVCP140.dll : ",errors.count_MSVCP140," | DxvkSubmissionQueue : ",errors.count_SubmissionQueue)
            Buffout_Trap = 0     
        if (errors.count_DXGIAdapter or errors.count_DXGIFactory) >= 1:
            print("Checking for Vulkan Settings Crash........CULPRIT FOUND!")
            print("> Priority Level: [5] | dxvk::DXGIAdapter : ",errors.count_DXGIAdapter," | dxvk::DXGIFactory : ",errors.count_DXGIFactory)
            Buffout_Trap = 0        
        if (errors.count_BSXAudio2Data or errors.count_BSXAudio2Game) >= 1:
            print("Checking for Corrupted Audio Crash........CULPRIT FOUND!")
            print("> Priority Level: [4] | BSXAudio2DataSrc : ",errors.count_BSXAudio2Data," | BSXAudio2GameSound : ",errors.count_BSXAudio2Game)
            Buffout_Trap = 0
        if (errors.count_CompileAndRun or errors.count_NiBinaryStream or errors.count_ConsoleLogPrinter) >= 1:
            print("Checking for Console Command Crash........CULPRIT FOUND!")
            print("> Priority Level: [1] | SysWindowCompileAndRun : ",errors.count_CompileAndRun," | BSResourceNiBinaryStream : ",errors.count_NiBinaryStream," | ConsoleLogPrinter : ",errors.count_ConsoleLogPrinter)
            Buffout_Trap = 0 
        if errors.count_ParticleSystem >= 1:
            print("Checking for Particle Effects Crash.......CULPRIT FOUND!")
            print("> Priority Level: [4] | ParticleSystem : ",errors.count_ParticleSystem)
            Buffout_Trap = 0            
        if errors.count_Anim1 or errors.count_Anim2 or errors.count_Anim3 or errors.count_Anim4 >= 1:
            print("Checking for Animation / Physics Crash....CULPRIT FOUND!")
            print("> Priority Level: [5] | hkbVariableBindingSet : ",errors.count_Anim1," | hkbHandIkControlsModifier : ",errors.count_Anim2)
            print("                        hkbBehaviorGraph : ",errors.count_Anim3," | hkbModifierList : ",errors.count_Anim4)
            Buffout_Trap = 0            
        if errors.count_DLCBanner05 >= 1:
            print("Checking for Archive Invalidation Crash...CULPRIT FOUND!")
            print("> Priority Level: [5] | DLCBanner05.dds : ",errors.count_DLCBanner05)
            Buffout_Trap = 0
        print("---------- Unsolved Crash Messages Below ----------")
        if (errors.count_BGSAttachment or errors.count_BGSTemplate or errors.count_BGSTemplateItem) >= 1:
            print("Checking for *[Item Crash]................DETECTED!")
            print("> Priority Level: [5] | BGSMod::Attachment : ",errors.count_BGSAttachment," | BGSMod::Template : ",errors.count_BGSTemplate," | BGSMod::Template::Item : ",errors.count_BGSTemplateItem)
            Buffout_Trap = 0           
        if errors.count_BGSSaveBuffer >= 2:
            print("Checking for *[Save Crash]................DETECTED!")
            print("> Priority Level: [5] | BGSSaveFormBuffer : ",errors.count_BGSSaveBuffer)
            Buffout_Trap = 0
        if (errors.count_ButtonEvent or errors.count_MenuControls or errors.count_MenuOpenClose or errors.count_PlayerControls or errors.count_DXGISwapChain) >= 1:
            print("Checking for *[Input Crash]...............DETECTED!")
            print("> Priority Level: [5] | ButtonEvent : ",errors.count_ButtonEvent," | MenuControls : ",errors.count_MenuControls)
            print("                        MenuOpenCloseHandler : ",errors.count_MenuOpenClose," | PlayerControls : ",errors.count_PlayerControls," | DXGISwapChain : ",errors.count_DXGISwapChain)
            Buffout_Trap = 0
        if (errors.count_BGSSaveManager or errors.count_BGSSaveThread or errors.count_BGSSaveBuffer or errors.count_INIMem1 or errors.count_INIMem2) >= 1:
            print("Checking for *[Bad INI Crash].............DETECTED!")
            print("> Priority Level: [5] | BGSSaveLoadManager : ",errors.count_BGSSaveManager," | BGSSaveLoadThread : ",errors.count_BGSSaveThread," | BGSSaveFormBuffer : ",errors.count_BGSSaveBuffer)
            Buffout_Trap = 0
        if (errors.count_Patrol or errors.count_PatrolExec or errors.count_PatrolActor) >= 1: 
            print("Checking for *[NPC Patrol Crash]..........DETECTED!")
            print("> Priority Level: [5] | BGSProcedurePatrol : ",errors.count_Patrol," | BGSProcedurePatrolExecStatel : ",errors.count_PatrolExec," | PatrolActorPackageData : ",errors.count_PatrolActor)
            Buffout_Trap = 0            
        if (errors.count_BSPacked or errors.count_BGSCombined or errors.count_BGSStatic or errors.count_TESObjectCELL) >= 1:
            print("Checking for *[Precombines Crash].........DETECTED!")
            print("> Priority Level: [5] | BGSStaticCollection : ",errors.count_BGSStatic," | BGSCombinedCellGeometryDB : ",errors.count_BGSCombined)
            print("                        BSPackedCombinedGeomDataExtra : ",errors.count_BSPacked," | TESObjectCELL : ",errors.count_TESObjectCELL)
            Buffout_Trap = 0 
        if errors.count_HUDAmmo >= 1:
            print("Checking for *[Ammo Counter Crash]........DETECTED!")
            print("> Priority Level: [5] | HUDAmmoCounter : ",errors.count_HUDAmmo)
            Buffout_Trap = 0
        if (errors.count_BGSProjectile or errors.count_CombatProjectile) >= 1:
             print("Checking for *[NPC Projectile Crash].....DETECTED!")
             print("> Priority Level: [5] | BGSProjectile : ",errors.count_BGSProjectile," | CombatProjectileAimController : ",errors.count_CombatProjectile)
             Buffout_Trap = 0
        if (errors.count_Player and errors.count_0x7) >= 2 and (errors.count_0x14 or errors.count_0x8) >= 2:
            print("Checking for *[Player Character Crash]....DETECTED!")
            print("> Priority Level: [5] | PlayerCharacter : ",errors.count_Player," | 0x00000007 : ",errors.count_0x7)
            print("                        0x00000008 : ",errors.count_0x8," | 0x000000014 : ",errors.count_0x14)
            Buffout_Trap = 0

        if Buffout_Trap == 1:
            prints.buff_trap_correct()
        else:
            prints.buff_trap_incorrect()
        
        prints.hello_frequent_crashes()
        Mod_Trap1 = 1
        Mod_TrapX = 1
        no_repeat2 = 1
        
        errors_list: list[ModsCountChecker] = []
        i = 0
        while i != len(List_Mods1):
            errors_list.append(ModsCountChecker(
                f'{str(List_Mods1[i])}', f'{str(List_Warn1[i])}'
        ))
            i += 1
            
        for line in all_lines:
            for err in errors_list:
                if err(line):
                    Mod_Trap1 = 0
        errors_list.clear()

        if errors.count_CHW >= 2 and \
        (errors.count_BSShadow or errors.count_BSShader or errors.count_BSDFLight) >= 1 or str("ClassicHolsteredWeapons") in buff_error:
            prints.count_CHW_1_incorrect()
            Mod_Trap1 = 0
            Buffout_Trap = 0
        elif errors.count_CHW >= 2 and \
            (errors.count_UniquePlayer or errors.count_HighHeels or errors.count_CBP or errors.count_BodyNIF) >= 1:
            prints.count_CHW_2_incorrect()
            Mod_Trap1 = 0
            Buffout_Trap = 0
        elif errors.count_CHW == 1 and str("d3d11") in buff_error:
            prints.count_CHW_3_incorrect()
            Mod_Trap1 = 0
            
        #DEFINE CHECKS IF NOTHING TRIGGERED MOD TRAP 1 & NO PLUGINS

        if Mod_Trap1 == 0:
            prints.mods_trap_1()
        elif Mod_TrapX == 0:
            prints.mods_trap_2()
        elif Mod_Trap1 == 1:
            prints.mods_trap_3()

        prints.hello_community_patches()
        Mod_Trap2 = 1
        no_repeat1 = 1
        
        if errors.count_Unofficial == 0 and errors.count_LoadOrder == 0:
            Mod_TrapX = 0
        elif errors.count_Unofficial == 0 and errors.count_LoadOrder >= 1:
            prints.unofficial_patch_incorrect()
        
        i = 0
        while i != len(List_Mods2):
            errors_list.append(ModsCountChecker(
                f'{str(List_Mods2[i])}', f'{str(List_Warn2[i])}'
        ))
            i += 1
            
        for line in all_lines:
            for err in errors_list:
                if err(line):
                    Mod_Trap2 = 0
        
            if no_repeat1 == 1 and str("File:") not in line and ("Depravity.esp" in line or "FusionCityRising.esp" in line or "HotC.esp" in line or "OutcastsAndRemnants.esp" in line or "ProjectValkyrie.esp" in line):
                print("[!] Found:", line[0:9].strip(), "THUGGYSMURF QUEST MODS")
                prints.repeat_mods()
                no_repeat1 = 0
                Mod_Trap2 = 0
            
        errors_list.clear()
 
        if errors.count_FallSouls >= 1:
            prints.count_Fallsouls_incorrect()
            Mod_Trap2 = 0
        
        if Mod_TrapX == 0:
            prints.mods_trap_4()
            Mod_Trap2 = 0
        elif Mod_Trap2 == 1:
            prints.mods_trap_5()
        
        prints.hello_patched_through_opc()
        Mod_Trap3 = 1

        i = 0
        while i != len(List_Mods3):
            errors_list.append(ModsCountChecker(
                f'{str(List_Mods3[i])}', f'{str(List_Warn3[i])}'
        ))
            i += 1
            
        for line in all_lines:
            for err in errors_list:
                if err(line):
                    Mod_Trap3 = 0
        errors_list.clear()

        if Mod_TrapX == 0:
            prints.mods_trap_6()
        elif Mod_Trap3 == 0:
            prints.mods_trap_7()
        elif Mod_Trap3 == 1:
            prints.mods_trap_8()

        prints.hello_specific()
        list_DETPLUGINS = []
        list_DETFORMIDS = []
        list_DETFILES = []
        list_ALLPLUGINS = []

        if errors.count_F4SE == 0 and errors.count_Module >= 1:
            prints.extender()
            
        for line in all_lines:
            if len(line) >= 6 and "]" in line[4]:
                list_ALLPLUGINS.append(line.strip())
            if len(line) >= 7 and "]" in line[5]:
                list_ALLPLUGINS.append(line.strip())
            if len(line) >= 10 and "]" in line[8]:
                list_ALLPLUGINS.append(line.strip())
            if len(line) >= 11 and "]" in line[9]:
                list_ALLPLUGINS.append(line.strip())

        print("LIST OF (POSSIBLE) PLUGIN CULRIPTS:")
        for line in all_lines:
          if "File:" in line:
            line = line.replace("File: ", "") 
            line = line.replace('"', '') 
            list_DETPLUGINS.append(line.strip())

        list_DETPLUGINS = list(dict.fromkeys(list_DETPLUGINS))
        list_remove = [
            "Fallout4.esm", "DLCCoast.esm", 
            "DLCNukaWorld.esm", "DLCRobot.esm", 
            "DLCworkshop01.esm", "DLCworkshop02.esm", 
            "DLCworkshop03.esm", "", ''
        ]
        for elem in list_remove:
            if elem in list_DETPLUGINS:
                list_DETPLUGINS.remove(elem)
                
        PL_strings = list_ALLPLUGINS
        PL_substrings = list_DETPLUGINS
        PL_result = []

        for string in PL_strings:
            PL_matches = []
            for substring in PL_substrings:
                if substring in string:
                    PL_matches.append(string)
            if PL_matches:
                PL_result.append(PL_matches)
                print("- " + ' '.join(PL_matches))

        if not PL_result:
            prints.result_correct()
        else:
            prints.pl_result_incorrect()

        print("LIST OF (POSSIBLE) FORM ID CULRIPTS:")
        for line in all_lines:
          if "Form ID:" in line and not "0xFF" in line:
            line = line.replace("0x", "")
            list_DETFORMIDS.append(line.strip())
            line = line.replace("Form ID: ", "")
            line = line[:5].strip()

        list_DETFORMIDS = list(dict.fromkeys(list_DETFORMIDS))
        for elem in list_DETFORMIDS:
            print(elem)

        if not list_DETFORMIDS:
            prints.result_correct()
        else:
            prints.detf_incorrect()
        
        print("LIST OF (POSSIBLE) FILE CULPRITS:")
        List_Files = [
            ".bgsm",".bto",".btr",".dds",
            ".hkb",".hkx",".ini",".nif",
            ".pex",".swf",".txt",".uvd",
            ".wav",".xwm","data\*"
        ]
        
        for line in all_lines:
            for elem in List_Files:
                if elem in line.lower():
                    line = line.replace("File Name: ", "") 
                    line = line.replace("Name: ", "") 
                    line = line.replace('"', '')
                    list_DETFILES.append(line.strip())

        list_DETFILES = list(dict.fromkeys(list_DETFILES))
        for elem in list_DETFILES:
            print(elem)

        if not list_DETFILES:
            prints.result_correct()
        else:
            prints.detf_incorrect_2()

        prints.hello_authors()
        sys.stdout = orig_stdout
        result = True
        after_scanning(result, md_file_name, flag)
